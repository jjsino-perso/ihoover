import { assert, expect } from 'chai';
import { checkParameters, calculatePosition } from '../../src/commands/calculate-position/calculate-position';
import Hoover from '../../src/hoover/hoover';
import { EnumOrientation } from '../../src/data/orientation';
import { EnumMoveInstruction, EnumPivotInstruction } from '../../src/data/instruction';
import InvalidOptionError from '../../src/error/invalid-option-error';

describe('calculate-position', function() {
    
    const hoover: Hoover =  new Hoover(
        {x: 5, y: 5},
        EnumOrientation.north,
        {x: 10, y: 10}
    );
    const calculationOptions = {
        initState: hoover,
        instructions: [
            EnumPivotInstruction.right,
            EnumMoveInstruction.forward,
            EnumPivotInstruction.right,
            EnumMoveInstruction.forward,
            EnumPivotInstruction.right,
            EnumMoveInstruction.forward,
            EnumPivotInstruction.right,
            EnumMoveInstruction.forward,
            EnumMoveInstruction.forward,
        ]
    };

    describe('checkParameters', function() {

        it('Should prepare calculation options ', function() {
            assert.deepEqual(
                checkParameters({gridSize: '10,10', initialPosition: '5,5,N', instructions: 'DADADADAA'}),
                calculationOptions
            );
        });

        it('Should not validate grid size', function() {
            expect(() => checkParameters({gridSize: 'N,N', initialPosition:'N',instructions:'TT'}))
                .to.throw(InvalidOptionError);
        });


        it('Should not validate position', function() {
            expect(() => checkParameters({gridSize: '10,10', initialPosition:'N',instructions:'TT'}))
                .to.throw(InvalidOptionError);
        });

        it('Should not validate instructions', function() {
            expect(() => checkParameters({gridSize: '10,10', initialPosition:'5,5,S',instructions:'TT'}))
                .to.throw(InvalidOptionError);
        });

    });

    describe('checkParameters', function() {
    
        it('Should calculate the final position', function() {
            calculatePosition(calculationOptions);
            assert.deepEqual(calculationOptions.initState.getPosition(), {x: 5, y: 6});
            assert.deepEqual(calculationOptions.initState.getOrientation(), EnumOrientation.north);
        });

    });

  });