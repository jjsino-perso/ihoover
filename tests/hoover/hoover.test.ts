import { assert, expect } from 'chai';
import Hoover from '../../src/hoover/hoover';
import { EnumOrientation } from '../../src/data/orientation';
import { EnumMoveInstruction, EnumPivotInstruction } from '../../src/data/instruction';
import ExecutionError from '../../src/error/execution-error';

describe('hoover', function() {

    let hoover: Hoover;
    
    beforeEach(function() {
        hoover =  new Hoover(
            {x: 5, y: 5},
            EnumOrientation.north,
            {x: 10, y: 10}
        );
    });

    describe('pivot', function() {
        it('Should turn left to the west direction', function() {
            assert.equal(
                hoover.pivot(EnumPivotInstruction.left).getOrientation(),
                EnumOrientation.west
            );
        }); 

    });


    describe('move', function() {
        it('Should move forward into the north direction', function() {
            const actual = hoover.move(EnumMoveInstruction.forward);
            assert.equal(actual.getOrientation(), EnumOrientation.north);
            assert.deepEqual(actual.getPosition(), {x: 5, y: 6});
        }); 
    });

    describe('pivot-and-move', function() {
        it('Should turn right and move forward', function() {
            hoover.pivot(EnumPivotInstruction.right);
            assert.deepEqual(hoover.getOrientation(), EnumOrientation.east);
            hoover.move(EnumMoveInstruction.forward);
            assert.deepEqual(hoover.getPosition(), {x: 6, y: 5});
            assert.deepEqual(hoover.getOrientation(), EnumOrientation.east);
        });

        it('Should throw unexecutable error', function() {
            hoover.move(EnumMoveInstruction.forward);
            hoover.move(EnumMoveInstruction.forward);
            hoover.move(EnumMoveInstruction.forward);
            hoover.move(EnumMoveInstruction.forward);
            hoover.move(EnumMoveInstruction.forward);
            expect(() => hoover.move(EnumMoveInstruction.forward))
                .to.throw(ExecutionError);
        })
    })

  });