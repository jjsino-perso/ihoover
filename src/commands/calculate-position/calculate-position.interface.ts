import { EnumInstruction } from '../../data/instruction';
import { EnumOrientation } from "../../data/orientation";
import { IPosition } from '../../common/position';
import Hoover from '../../hoover/hoover';

export interface ICalculatePositionInput {
    gridSize: string;
    initialPosition: string;
    instructions: string;
}

export interface IGridSize {
    x: number;
    y: number;
}

export interface IState extends IPosition {
    orientation: EnumOrientation;
}

export interface ICalculateOptions {
    initState: Hoover,
    instructions: EnumInstruction[]
}
