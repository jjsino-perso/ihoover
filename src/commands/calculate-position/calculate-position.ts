import { EnumOrientation } from '../../data/orientation';
import { ICalculatePositionInput, IGridSize, ICalculateOptions, IState } from './calculate-position.interface';
import { EnumPivotInstruction, EnumMoveInstruction, EnumInstruction } from '../../data/instruction';
import { IPosition } from '../../common/position';
import Hoover from '../../hoover/hoover';
import Output from '../../output';
import InvalidOptionError from '../../error/invalid-option-error';


export function checkParameters(input: ICalculatePositionInput): ICalculateOptions {
    const gridSize: IPosition = checkGridSize(input.gridSize);
    const initState = checkPostition(input.initialPosition);
    const instructions = checkInstructions(input.instructions);
    
    Output.info(`Input information: `);
    Output.info(`Grid size: x=${gridSize.x}, y=${gridSize.y}`);
    Output.info(`Initial position: x=${initState.initialPosition.x}, y=${initState.initialPosition.y}, orientation=${initState.orientation}`);
    Output.info(`instructions: ${instructions.toString()}`);

    return {
        initState: new Hoover(
            initState.initialPosition,
            initState.orientation,
            gridSize
        ),
        instructions
    };
}

export function calculatePosition(input: ICalculateOptions): void {
    let hoover = input.initState;
    input.instructions.forEach((instruction: EnumInstruction) => {
        hoover = (  Object.values(EnumPivotInstruction).some(m => m === instruction) ) 
            ? hoover.pivot(instruction as EnumPivotInstruction) 
            : hoover.move(instruction as EnumMoveInstruction);
    });
    const position = hoover.getPosition();
    
    Output.success(`Final position: x=${position.x}, y=${position.y}, orientation=${hoover.getOrientation()}`);
}

function checkGridSize(size: string): IGridSize {
    const xySizes = size.split(',');
    try {
        return {
            x: checkPositionValue('Grid size', xySizes[0]), 
            y: checkPositionValue('Grid size', xySizes[1])
        };
    } catch (error) {
        throw new InvalidOptionError("grid size invalid");
    }
}

function checkPostition(position: string): {initialPosition: IPosition, orientation: EnumOrientation} {
    const positionData = position.split(',');
    try {
        return {
            initialPosition: { x: checkPositionValue('Position', positionData[0]), y: checkPositionValue('Position', positionData[1]) },
            orientation: positionData[2] as EnumOrientation
        };
    } catch (error) {
        throw new InvalidOptionError("Position and oriention options invalid");
    }
}

function checkInstructions(instructions: string): EnumInstruction[] {
    return Array.from(instructions).map((instruction: string) => {
        if( Object.values(EnumPivotInstruction).some(i => i === instruction) ) {
            return instruction as EnumPivotInstruction;
        }
        if(  Object.values(EnumMoveInstruction).some(i => i === instruction) ) {
            return instruction as EnumMoveInstruction;
        }
        throw new InvalidOptionError(`Instruction invalid`);
    });
}

function checkPositionValue(optionName: string, value: string): number {
    const result: number = parseInt(value);
    if (!isNaN(result)) {
        return result;
    }
    throw new InvalidOptionError(`${optionName} invalid`);
}