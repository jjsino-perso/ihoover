import colors from "colors";

export default {
    info(message: string) {
        console.info(colors.blue(message));
    },

    warn(message : string) {
        console.warn(colors.yellow(message));
    },

    success(message: string, ...args: any[]) {
        console.info(colors.green(message), ...args);
    },

    error(error: Error) {
        console.error(colors.red(error.message));
    }
}