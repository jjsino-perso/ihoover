import { EnumOrientation } from "../data/orientation";
import { EnumPivotInstruction, EnumMoveInstruction } from '../data/instruction';
import { IPosition } from '../common/position';
import ExecutionError from '../error/execution-error';

export default class Hoover {

    public constructor(
        private position: IPosition,
        private orientation: EnumOrientation,
        private edge: IPosition
    ) {

    }

    public pivot(command: EnumPivotInstruction): this {
        const orientations: EnumOrientation[] = buildPivotMap().get(command) ||  [];
        const currentOrientationIndex: number = orientations.findIndex((orientation: EnumOrientation) => orientation === this.orientation);
        this.orientation = orientations[currentOrientationIndex+1];
        return this;
    }

    public move(command: EnumMoveInstruction): this {
        const execute = buildMovementMap().get(this.orientation); 
        if (!execute) {
            throw new Error('Internal error');
        }
        const position = execute(this.position, command);
        
        if (this.isOutOfEdge(position)) {
            throw new ExecutionError('Instructions are not executable : hoover got out of edge');
        }
        
        this.position = position;
        return this;
    }

    public getPosition() {
        return this.position;
    }

    public getOrientation() {
        return this.orientation;
    }

    private isOutOfEdge(position: IPosition): boolean {
        return position.x < 0
            || position.y < 0 
            || position.x > this.edge.x
            || position.y > this.edge.y;
    }
}

function buildPivotMap(): Map<EnumPivotInstruction,EnumOrientation[]> {
    const pivotMap: Map<EnumPivotInstruction,EnumOrientation[]> = new Map();
    pivotMap.set(EnumPivotInstruction.left, [
        EnumOrientation.north,
        EnumOrientation.west,
        EnumOrientation.south,
        EnumOrientation.east,
        EnumOrientation.north,
    ]);
    pivotMap.set(EnumPivotInstruction.right, [
        EnumOrientation.north,
        EnumOrientation.east,
        EnumOrientation.south,
        EnumOrientation.west,
        EnumOrientation.north,
    ]);
    return pivotMap;
}

function buildMovementMap(): Map<EnumOrientation, (p: IPosition, i: EnumMoveInstruction) => IPosition> {
    const movementMap = new Map();
    movementMap.set(EnumOrientation.north, (p: IPosition) => ({x: p.x, y: ++p.y}) );
    movementMap.set(EnumOrientation.east, (p: IPosition) => ({x: ++p.x, y: p.y}) );
    movementMap.set(EnumOrientation.south, (p: IPosition) => ({x: p.x, y: --p.y}) );
    movementMap.set(EnumOrientation.west, (p: IPosition) => ({x: --p.x, y: p.y}) );
    return movementMap;
}