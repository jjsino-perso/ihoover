export default class InvalidOptionError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = "InvalidOptionError"; 
    }
}