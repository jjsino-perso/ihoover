import { readFileSync } from "fs";
import { join } from "path";
import commander from "commander";
import Output from './output';
import { checkParameters, calculatePosition } from './commands/calculate-position/calculate-position';

const packageJSON = JSON.parse(
    readFileSync(join(__dirname, '..', 'package.json'), {encoding: 'utf8'})
);

commander.version(packageJSON.version);

createCommand("calculate-position", "Calculate position")
    .option("-s, --grid-size <gridSize>"
        , "X and Y size of the grid, ex: '10,10' for x=10,y=10 ")
    .option("-p, --initial-position <initialPosition>"
        , "initial position (X,Y) and orientation (N,E,S,W), ex: '5,5,N' for x=5,y=5,orientation=N")
    .option("-i, --instructions <instructions>"
        , "instructions to pivot or move forward the hoover, ex: 'DAG' for 'right, move, left'")
    .addHelpText('afterAll', 'Example call: calculate-position -p 5,5,N -s 10,10 -i DADADAA')
    .action(executeCommand(checkParameters, calculatePosition))
    .outputHelp();

commander
    .parse(process.argv);

function createCommand(command: string, description: string): commander.Command {
    return commander.command(command).description(description);
}

function executeCommand(
    checkCallback: (args: any) => any,
    actionCallback: (options: any) => void
) {
    return async(args: any) => {     
        let opt: any;
        try {
            opt = checkCallback(args);
        }catch(error) {
            Output.error(error);
            process.exit(1);
        }

        try {
            actionCallback(opt);
        } catch(error) {
            Output.error(error);
            process.exit(1);
        }
    }
}
