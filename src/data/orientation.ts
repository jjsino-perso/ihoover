
export enum EnumOrientation {
    north = "N",
    south = "S",
    west = "W",
    east = "E"
}