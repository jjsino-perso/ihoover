export enum EnumPivotInstruction  {
    left = "G",
    right = "D"
}

export enum EnumMoveInstruction {
    forward = "A"
}

export type EnumInstruction = EnumMoveInstruction | EnumPivotInstruction;