# Yanport IHoover Test 


## Requirements

1. npm
2. node

## Installation

1. Clone the repository
2. run `npm install -- production`
3. run `sudo npm link` to enable the command `yp-hoover calculate-position` from anywhere, otherwise you will have to launch `npm start -- calculate-position` from this project folder.

Example : 
`yp-hoover calculate-position -p 5,5,N -s 10,10 -i DADADADAA`

## Command help

`yp-hoover --help`

## Arguments for calculate-position

-   `-s, --grid-size <gridSize>` : X and Y size of the grid, ex: '10,10' for x=10,y=10 "
-   `-p, --initial-position <initialPosition>` : initial position (X,Y) and orientation (N,E,S,W), ex: '5,5,N' for x=5,y=5,orientation=N
-   `-i, --instructions <instructions>` : instructions to pivot or move forward the hoover, ex: 'DAG' for 'right, move, left'

## Run tests

`npm test`

